import React from "react";
import {Image} from 'react-native'
const img= require('../images/logo.png')

export default class LogoTitle extends React.Component {
  render() {
    return (
      <Image
        source={img}
        style={{ width: 45, height: 45, margin: 6 }}
      />
    );
  }
}