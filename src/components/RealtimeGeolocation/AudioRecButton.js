import React from "react";
import { Text, Button } from "native-base";
import { StyleSheet } from "react-native";

export default class AudioRecButton extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      drawerOpen: false,
      drawerDisabled: false
    };
  }

  render() {
    return (
      <Button
        rounded
        // large
        onPress={this.props.onPressHandler}
        style={styles.buttonStyle}
      >
        <Text style={styles.buttonText}>{this.props.buttonText}</Text>
      </Button>
    );
  }
}

var styles = StyleSheet.create({
  buttonStyle: {
    borderRadius: 50,
    alignSelf: "stretch",
    justifyContent: "center",
    backgroundColor: "#E22E40",
    borderColor: "#082E40",
    borderWidth: 5,
    margin: 7,
    height:50
  },
  buttonText: {
    fontSize: 16,
    fontFamily: "Gill Sans",
    textAlign: "center",
    margin: 5,
    color: "white",
    backgroundColor: "transparent"
  }
});
