import React from "react";
import { Spinner, Content } from "native-base";

export default class AppSpinner extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <Content>
        <Spinner color="blue" />
      </Content>
    );
  }
}
