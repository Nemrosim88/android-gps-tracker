import React from "react";
import { Text, Container } from "native-base";
import { StyleSheet } from "react-native";

class GeolocationText extends React.Component {
  render() {
    return (
      <Container style={{ backgroundColor: "trabsparent" }}>
        <Text style={styles.textStyle}>Latitude: {this.props.latitude}</Text>
        <Text style={styles.textStyle}>Longitude: {this.props.longitude}</Text>

        <Text style={styles.textStyle}>Time updated: </Text>
        <Text style={styles.textStyleGreen}>{this.props.time}</Text>

        {this.props.error ? <Text>Error: {this.props.error}</Text> : null}

        <Text style={styles.textStyle}>Server message:</Text>
        <Text style={styles.textStyleGreen}>{this.props.serverMessage}</Text>
      </Container>
    );
  }
}

export default GeolocationText;

var styles = StyleSheet.create({
  textStyle: {
    fontSize: 16,
    fontFamily: "Gill Sans",
    textAlign: "center",
    margin: 5,
    color: "white",
    backgroundColor: "transparent"
  },
  textStyleGreen: {
    fontSize: 16,
    fontFamily: "Gill Sans",
    textAlign: "center",
    margin: 5,
    color: "#38C340"
  },
  textStyleRed: {
    fontSize: 16,
    fontFamily: "Gill Sans",
    textAlign: "center",
    margin: 5,
    color: "#DE2E40"
  }
});
