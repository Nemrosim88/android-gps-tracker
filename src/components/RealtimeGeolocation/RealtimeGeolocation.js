import React from "react";
import { PermissionsAndroid, Alert, NetInfo, AsyncStorage } from "react-native";
import { Container, Toast, Grid, Col } from "native-base";
import axios from "axios";

import GeolocationButton from "./GeolocationButton";
import AudioRecButton from "./AudioRecButton";
import AppSpinner from "./AppSpinner";
import GeolocationText from "./GeolocationText";

class RealtimeGeolocation extends React.Component {
  constructor(props) {
    super(props);

    this.startLocationHandler = this.startLocationHandler.bind(this);
    this.stopLocationHandler = this.stopLocationHandler.bind(this);
    this.requestLocationPermission();

    this.state = {
      latitude: null,
      longitude: null,
      time: null,
      serverMessage: null,
      error: null,
      showToast: false,
      loading: false,
      logged: false,
      user: {
        name: null,
        surname: null,
        patronymic: null,
        email: null,
        childKey: null
      }
    };
  }

  async requestLocationPermission() {
    try {
      const granted = await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
        {
          title: "MapApp GPS Permission",
          message:
            "MapApp needs access to your geolocation hardware " +
            "so you can send location to server."
        }
      );
      if (granted === PermissionsAndroid.RESULTS.GRANTED) {
        console.log("You can use the geolocation");
      } else {
        console.log("Geolocation permission denied");
      }
    } catch (err) {
      console.warn(err);
    }
  }

  async requestInternetPermission() {
    try {
      const granted = await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.PERMISSIONS.READ_PHONE_STATE,
        {
          title: "MapApp PhoneState Permission",
          message: "MapApp needs access to your Phone State."
        }
      );
      if (granted === PermissionsAndroid.RESULTS.GRANTED) {
        console.log("You can use the geolocation");
      } else {
        console.log("Geolocation permission denied");
      }
    } catch (err) {
      console.warn(err);
    }
  }

  /**
   * Checks, if device is connected to internet or not.
   * If not -> Shows Toast with request to switch on internet connection.
   */
  async getInternetConnectionType() {
    NetInfo.getConnectionInfo()
      .then(connectionInfo => {
        if (connectionInfo.type === "none") {
          Toast.show({
            text: "Please turn on internet connection",
            buttonText: "Got it",
            buttonTextStyle: { color: "#008000" },
            buttonStyle: { backgroundColor: "#5cb85c" },
            duration: 3000
          });
        }
      })
      .catch(error => {
        console.log(error);
        Toast.show({
          text: "Something went wront with internet connection info",
          buttonText: "Got it",
          buttonTextStyle: { color: "red" },
          buttonStyle: { backgroundColor: "#5cb85c" },
          duration: 3000
        });
      });
  }

  componentWillUnmount() {
    navigator.geolocation.clearWatch(this.watchId);
    navigator.geolocation.stopObserving();
  }

  getTextRepresentationOfCurrentTime(currentDate) {
    var datetime =
      "Last Sync: " +
      currentDate.getDate() +
      "/" +
      (currentDate.getMonth() + 1) +
      "/" +
      currentDate.getFullYear() +
      " @ " +
      currentDate.getHours() +
      ":" +
      currentDate.getMinutes() +
      ":" +
      currentDate.getSeconds();

    return datetime;
  }

  loadUser() {
    try {
      const userFromStorage = AsyncStorage.getItem("user")
        .then(result => {
          if (result !== null) {
            const user = JSON.parse(result);

            console.log("USERRRRRRRRRRRRR   !!!!!!!!!!!", user);

            this.setState(prevState => ({
              user: {
                ...prevState.user,
                name: user.name,
                surname: user.surname,
                patronymic: user.patronymic,
                email: user.email,
                childKey: user.childKey
              },
              logged: true
            }));

            Toast.show({
              text: "Logged",
              buttonText: "Got it",
              buttonTextStyle: { color: "#008000" },
              buttonStyle: { backgroundColor: "#5cb85c" },
              duration: 3000
            });
          } else {
            Toast.show({
              text: "Please login",
              buttonText: "Got it",
              buttonTextStyle: { color: "#008000" },
              buttonStyle: { backgroundColor: "#5cb85c" },
              duration: 3000
            });
          }
        })
        .catch(error => {
          console.log(error.message);
        });
        return true;
    } catch (error) {
      console.log(error.message);
    }
  }

  startLocationHandler() {
    // this.loadUser();

    console.log("LOGGED !!!!!!!!!!!!!!!!!!!!: ", this.state.logged);
    if (this.loadUser()) {
      console.log("STARTING !!!!!!!!!!!!!!!!!!!!!!!!!!!!");
      Toast.show({
        text: "Starting...",
        buttonText: "Got it",
        buttonTextStyle: { color: "red" },
        buttonStyle: { backgroundColor: "#5cb85c" },
        duration: 3000
      });

      this.requestLocationPermission();
      this.getInternetConnectionType();

      this.watchId = navigator.geolocation.watchPosition(
        position => {
          this.setState({ loading: true });
          var currentdate = new Date();
          var datetime = this.getTextRepresentationOfCurrentTime(currentdate);

          axios
            .post("https://node-react-first.firebaseio.com/coordinates.json", {
              latitude: position.coords.latitude,
              longitude: position.coords.longitude,
              time: new Date(),
              userKey: this.state.user.childKey,
              userEmail: this.state.user.email
            })
            .then(response => {
              this.setState({
                loading: false,
                serverMessage:
                  "Coordinates successfully sent to server \n" +
                  "status code: " +
                  response.status +
                  "\n" +
                  'data: Long "' +
                  JSON.parse(response.config.data).longitude.toFixed(6) +
                  '"\n' +
                  'data: Lat "' +
                  JSON.parse(response.config.data).latitude.toFixed(6) +
                  '"\n' +
                  'time: "' +
                  datetime +
                  '"\n'
              });
            })
            .catch(error =>
              this.setState({
                loading: false,
                serverMessage: "Coordinates wasnt sent to server"
              })
            );

          this.setState({
            latitude: position.coords.latitude,
            longitude: position.coords.longitude,
            time: datetime,
            error: null
          });
          // this.setState({ loading: false });
        },
        error => this.setState({ error: error.message }),
        {
          enableHighAccuracy: true,
          timeout: 100,
          maximumAge: 10000,
          distanceFilter: 0
        }
      );
    } else {
      Toast.show({
        text: "Please login",
        buttonText: "Got it",
        buttonTextStyle: { color: "#008000" },
        buttonStyle: { backgroundColor: "#5cb85c" },
        duration: 2000
      });
    }
  }

  stopLocationHandler() {
    navigator.geolocation.clearWatch(this.watchId);
    navigator.geolocation.stopObserving();

    Toast.show({
      text: "Geolocation stoped",
      buttonText: "Got it",
      buttonTextStyle: { color: "#008000" },
      buttonStyle: { backgroundColor: "#5cb85c" },
      duration: 3000
    });
  }

  render() {
    return (
      <Container style={{ backgroundColor: "transparent", padding: 15 }}>
        <Container style={{ flex: 0.2, backgroundColor: "transparent" }}>
          <Grid>
            <Col>
              <GeolocationButton
                onPressHandler={e => this.startLocationHandler(e)}
                buttonText={"Start geolocation"}
              />
            </Col>
            <Col>
              <GeolocationButton
                onPressHandler={e => this.stopLocationHandler(e)}
                buttonText={"Stop geolocation"}
              />
            </Col>
          </Grid>
        </Container>
        <Container style={{ flex: 0.8, backgroundColor: "transparent" }}>
          <Container style={{ flex: 0.7, backgroundColor: "transparent" }}>
            <AudioRecButton
              onPressHandler={() => {
                Alert.alert("In development");
              }}
              buttonText={"Rec Audio"}
            />

            <GeolocationText
              latitude={this.state.latitude}
              longitude={this.state.longitude}
              time={this.state.time}
              error={this.state.error}
              serverMessage={this.state.serverMessage}
            />
          </Container>
          <Container style={{ flex: 0.3, backgroundColor: "transparent" }}>
            {this.state.loading ? <AppSpinner /> : null}
          </Container>
        </Container>
      </Container>
    );
  }
}

export default RealtimeGeolocation;
