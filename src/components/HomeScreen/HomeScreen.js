import React from "react";
import { Container, Button, Text } from "native-base";
import LinearGradient from "react-native-linear-gradient";
import { StyleSheet, AsyncStorage, Alert } from "react-native";

import RealtimeGeolocation from "../RealtimeGeolocation/RealtimeGeolocation";
import AppDrawer from "../Drawer/AppDrawer";

export default class HomeScreen extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      backgroundColors_v1: ["#115E8A", "#124766", "#051B29"],
      backgroundColors_v2: ["#C16B00", "#C19C62", "#051B29"]
    };
  }

  static navigationOptions = ({ navigation }) => {
    return {
      headerRight: (
        <Button
          style={{ backgroundColor: "transparent", margin: 6 }}
          onPress={() => navigation.navigate("LoginForm")}
        >
          <Text>Login</Text>
        </Button>
      )
    };
  };

  componentDidMount() {
    try {
      const userId = AsyncStorage.clear();
      console.log(userId);
    } catch (error) {
      console.log(error.message);
    }
  }

  async loadUserId() {
    try {
      const userId = await AsyncStorage.setItem("data");
      Alert.alert(userId);
    } catch (error) {
      console.log(error.message);
    }
  }

  render() {
    return (
      <AppDrawer>
        <Container style={{ flex: 1 }}>
          <LinearGradient
            colors={this.state.backgroundColors_v1}
            style={styles.linearGradient}
          >
            <Container style={styles.firstContainer}>
              <RealtimeGeolocation />
            </Container>

            <Container style={styles.secondContainer} />
          </LinearGradient>
        </Container>
      </AppDrawer>
    );
  }
}

var styles = StyleSheet.create({
  linearGradient: {
    flex: 1
    // borderRadius: 10,
    // padding: 2
  },
  buttonStyle: {
    alignSelf: "stretch",
    justifyContent: "center",
    backgroundColor: "#2c52ba"
  },
  buttonText: {
    fontSize: 18,
    fontFamily: "Gill Sans",
    textAlign: "center",
    margin: 10,
    color: "#ffffff",
    backgroundColor: "transparent"
  },
  firstContainer: {
    flex: 0.75,
    backgroundColor: "transparent"
  },
  secondContainer: {
    flex: 0.25,
    backgroundColor: "transparent",
    padding: 20
  }
});
