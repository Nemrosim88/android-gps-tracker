import React from "react";
import {
  Container,
  Content,
  Form,
  Item,
  Input,
  Label,
  Icon,
  Button,
  Text
} from "native-base";
import axios from "axios";
import { Alert, NetInfo } from "react-native";

export default class RegisterForm extends React.Component {
  constructor(props) {
    super(props);
    this.registerButtonHandler = this.registerButtonHandler.bind(this);
    this.checkPasswordMatch = this.checkPasswordMatch.bind(this);
  }

  state = {
    name: "",
    surname: "",
    patronymic: "",
    password: "",
    r_password: "",
    email: "",
    passwordsDontMatch: false,
    successfullySend: false,
    serverMessage: ""
  };

  registerButtonHandler(event) {
    if (NetInfo.isConnected) {
      NetInfo.getConnectionInfo().then(connectionInfo => {
        console.log(
          "Connection type: " +
            connectionInfo.type +
            ", effectiveType: " +
            connectionInfo.effectiveType
        );
      });

      if (this.state.name === "") {
        Alert.alert('Please, fill "name" field');
      } else if (this.state.surname === "") {
        Alert.alert('Please, fill "surname" field');
      } else if (this.state.password === "" && this.state.r_password === "") {
        Alert.alert('Please, fill "password" and "r_password" fields');
      } else if (this.state.email === "") {
        Alert.alert('Please, fill "surname" field');
      } else {
        const regDate = new Date();

        axios
          .post("https://node-react-first.firebaseio.com/users.json", {
            credentials: {
              name: this.state.name,
              surname: this.state.surname,
              patronymic: this.state.patronymic,
              email: this.state.email,
              password: this.state.password
            },
            regDate: {
              time: regDate.getTime(),
              year: regDate.getFullYear(),
              month: regDate.getMonth() + 1,
              day: regDate.getDate(),
              hour: regDate.getHours(),
              minute: regDate.getMinutes(),
              second: regDate.getSeconds(),
              millliseconds: regDate.getMilliseconds(),
              timezoneOffset: regDate.getTimezoneOffset(),
              UTCHours: regDate.getUTCHours()
            }
          })
          .then(response => {
            Alert.alert(
              "Send: Response status: " + JSON.stringify(response.status)
            );

            this.setState({
              serverMessage:
                "Successfully sent to server \n" +
                "status code: " +
                response.status +
                '"\n'
            });
          })
          .catch(error => {
            Alert.alert(JSON.stringify(error));

            this.setState({
              serverMessage: "Coordinates wasnt sent to server"
            });
          });
      }
    } else {
      Alert.alert("Please, turn on internet connection");
    }
  }

  checkPasswordMatch(string) {
    console.log("PASSWORD: ", string);
    const newPassword = string;
    this.setState({ r_password: newPassword });

    if (this.state.password !== newPassword) {
      this.setState({ passwordsDontMatch: true });
      console.log("TRUE!!!!!!!!!!");
    } else {
      this.setState({ passwordsDontMatch: false });
      console.log("FALSE!!!!!!!!!!");
    }
  }

  render() {
    return (
      <Container style={{ justifyContent: "center" }}>
        <Content>
          <Form style={{ alignItems: "center" }}>
            {/* The "floatingLabel" property creates an Input component, 
            whose Label animates upward when input is selected and animates downward when input is erased. */}
           
            <Item floatingLabel>
              <Label>Name</Label>
              <Input onChangeText={name => this.setState({ name })} />
            </Item>

            <Item floatingLabel>
              <Label>Surname</Label>
              <Input onChangeText={surname => this.setState({ surname })} />
            </Item>

            <Item floatingLabel>
              <Label>Patronymic</Label>
              <Input
                onChangeText={patronymic => this.setState({ patronymic })}
              />
            </Item>

            <Item floatingLabel>
              <Label>Password</Label>
              <Input
                secureTextEntry={true}
                onChangeText={password => this.setState({ password })}
              />
            </Item>

            {/* <Item floatingLabel error={this.state.passwordsDontMatch}> */}
            <Item floatingLabel error={this.state.passwordsDontMatch}>
              <Label>Repeat password</Label>
              <Input
                secureTextEntry={true}
                onChangeText={string => this.checkPasswordMatch(string)}
              />
              {this.state.passwordsDontMatch ? (
                <Icon name="close-circle" />
              ) : null}
            </Item>

            <Item floatingLabel>
              <Label>Email</Label>
              <Input onChangeText={email => this.setState({ email })} />
            </Item>
            <Text />
            <Button
              rounded
              style={{ alignSelf: "center" }}
              onPress={e => this.registerButtonHandler(e)}
            >
              <Text>Register</Text>
            </Button>
          </Form>
        </Content>
      </Container>
    );
  }
}
