import React from "react";
import {
  Item,
  Input,
  Label,
  Icon,
} from "native-base";

const RegisterForm = props => {
  return (
    <Item floatingLabel>
      <Label>{props.label}</Label>
      <Input
        // onChangeText={name => this.setState({ name })}
        // value={this.state.name}
      />

      <Icon name="checkmark-circle" />
    </Item>
  );
};

export default RegisterForm;
