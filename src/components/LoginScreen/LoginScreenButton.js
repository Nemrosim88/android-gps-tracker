import React from "react";
import { Button, Text } from "native-base";
import { StyleSheet } from "react-native";

export default class LoginScreenButton extends React.Component {

  render() {
    return (      
              <Button
                rounded
                title="Go to RegisterFormForm"
                onPress={this.props.onPress}
                style={styles.buttonStyle}
              >
                <Text style={styles.buttonText}>{this.props.buttonText}</Text>
              </Button>             
    );
  }
}

var styles = StyleSheet.create({
  buttonStyle: {
    alignSelf: "stretch",
    justifyContent: "center",
    backgroundColor: "#13658D",
    margin: 10,
  },
  buttonText: {
    fontSize: 18,
    fontFamily: "Gill Sans",
    textAlign: "center",
    margin: 5,
    color: "white",
    backgroundColor: "transparent"
  }
});
