import React from "react";
import {
  Container,
  Content,
  Form,
  Item,
  Input,
  Label,
  Toast
} from "native-base";
import { Alert, AsyncStorage } from "react-native";
import HomeScreenButton from "./LoginScreenButton";
import firebase from "react-native-firebase";

export default class RegisterForm extends React.Component {
  constructor(props) {
    super(props);
  }

  state = {
    login: "",
    password: "",
    result: null
  };

  async getAll() {
    let a = firebase.database().ref("/users");
    console.log(
      a.on("value", snapshot => {
        let data = snapshot.val();
        let items = Object.values(data);
        console.log("!!!!!!!!!ITEMS", items);
        this.setState({ items });
      })
    );
  }

  userEnteredLoginAndPassword() {
    if (this.state.login === "") {
      Toast.show({
        text: "Please enter login",
        duration: 3000
      });
      return false;
    } else if (this.state.password === "") {
      Toast.show({
        text: "Please enter password",
        duration: 3000
      });
      return false;
    } else {
      return true;
    }
  }

  // Если ничего не найдено
  // 11-04 08:02:06.901 23162-23601/com.mapapp I/ReactNativeJS: '!!! _Value: ', null
  // 11-04 08:02:06.901 23162-23601/com.mapapp I/ReactNativeJS: '!!!! CHILD: ', []

  // Если найдено
  // 11-04 08:02:55.637 23162-23601/com.mapapp I/ReactNativeJS: '!!! _Value: ', { '-LQGghZz1fmRjoHLydwm': { regDate: .... }}
  // 11-04 08:02:55.637 23162-23601/com.mapapp I/ReactNativeJS: '!!!! CHILD: ', [ '-LQGghZz1fmRjoHLydwm' ]

  async getUserDataFromDB() {
    if (this.userEnteredLoginAndPassword()) {
      let user = {
        name: null,
        surname: null,
        patronymic: null,
        email: null,
        childKey: null
      };

      let password = null;

      /**
       * Setting DB params.
       */
      var database = firebase.database().ref("/users");
      var query = database
        .orderByChild("credentials/email")
        .equalTo(this.state.login);

      /**
       * Getting data from query and setting values to variables to save in AsyncStorage.
       */
      query
        .once("value", snapshot => {
          this.setState({ result: snapshot._value });
          snapshot.forEach(child => {
            user.name = child.val().credentials.name;
            user.surname = child.val().credentials.surname;
            user.patronymic = child.val().credentials.patronymic;
            user.email = child.val().credentials.email;
            user.childKey = child.key;
            password = child.val().credentials.password;
          });
        })
        .then(result => {
          console.log("[MapApp]", this.state.result);

          console.log(result);
          if (this.state.result === null) {
            Toast.show({
              text: "Email was not found in database",
              buttonText: "Got it",
              buttonTextStyle: { color: "#008000" },
              buttonStyle: { backgroundColor: "#5cb85c" },
              duration: 2000
            });
          } else {
            if (password !== this.state.password) {
              Toast.show({
                text: "Wrong password",
                buttonText: "Got it",
                buttonTextStyle: { color: "#008000" },
                buttonStyle: { backgroundColor: "#5cb85c" },
                duration: 2000
              });
            } else {
              try {
                const hello = AsyncStorage.setItem("user", JSON.stringify(user))
                  .then(result => {
                    Toast.show({
                      text: "Successfully logged",
                      buttonText: "Got it",
                      buttonTextStyle: { color: "#008000" },
                      buttonStyle: { backgroundColor: "#5cb85c" },
                      duration: 2000
                    });
                  })
                  .catch(error => {
                    console.log(error.message);
                    Toast.show({
                      text: "Data was not saved to the app",
                      buttonText: "Got it",
                      buttonTextStyle: { color: "#008000" },
                      buttonStyle: { backgroundColor: "#5cb85c" },
                      duration: 2000
                    });
                  });
              } catch (error) {
                Alert.alert(JSON.stringify(error.message));
              }
            }
          }
        })
        .catch(error => {
          console.log(error.message);
        });
    }
  }

  async loadUserId() {
    try {
      const userId = await AsyncStorage.setItem("data");
      Alert.alert(userId);
    } catch (error) {
      console.log(error.message);
    }
  }

  componentWillMount() {
    this.removeUserId();
  }

  async removeUserId() {
    try {
      const userId = await AsyncStorage.removeItem("data");
    } catch (error) {
      console.log(error.message);
    }
  }

  render() {
    return (
      <Container style={{ justifyContent: "center" }}>
        <Content>
          <Form style={{ alignItems: "center" }}>
            <Item floatingLabel>
              <Label style={{}}>Login(Email)</Label>
              <Input onChangeText={login => this.setState({ login })} />
            </Item>

            <Item floatingLabel>
              <Label>Password</Label>
              <Input
                secureTextEntry={true}
                onChangeText={password => this.setState({ password })}
              />
            </Item>

            <HomeScreenButton
              onPress={e => this.getUserDataFromDB(e)}
              buttonText={"Login"}
            />

            <HomeScreenButton
              onPress={() => this.props.navigation.navigate("RegisterForm")}
              buttonText={"Register"}
            />
          </Form>
        </Content>
      </Container>
    );
  }
}
