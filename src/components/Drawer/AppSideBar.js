import React from "react";
import { Container, View, Text, Grid, Row, Icon } from "native-base";
import { StyleSheet, ImageBackground } from "react-native";

const image = require("../../images/blue-back.jpg");

class AppSideBar extends React.Component {
  render() {
    return (
      <View style={{ flex: 1, backgroundColor: "grey" }}>
        <Container style={{ flex: 0.3, backgroundColor: "trabsparent" }}>
          <ImageBackground source={image} style={styles.backgroundImage}>
            <Text style={styles.textStyleHeader}>MapApp</Text>
          </ImageBackground>
        </Container>

        <Grid>
          <Text style={styles.textStyle}>Profile Info</Text>
          <Icon name="person" style={{fontSize: 80, color: '#114975', alignSelf: "center"}}/>
          <Row style={styles.column}>
            <Text style={styles.textStyle}>Name</Text>
          </Row>
          <Row style={styles.column}>
            <Text style={styles.textStyle}>Surname</Text>
          </Row>
          <Row style={styles.column}>
            <Text style={styles.textStyle}>Patronymic</Text>
          </Row>
        </Grid>
      </View>
    );
  }
}

export default AppSideBar;

const styles = StyleSheet.create({
  backgroundImage: {
    flex: 1,
    width: null,
    height: null,
    resizeMode: "cover"
  },
  column: {
    backgroundColor: "white"
  },
  textStyle: {
    fontSize: 18,
    fontFamily: "Gill Sans",
    textAlign: "center",
    margin: 5,
    color: "black",
    backgroundColor: "transparent"
  },
  textStyleHeader: {
    fontSize: 24,
    fontFamily: "Gill Sans",
    textAlign: "center",
    margin: 40,
    color: "white",
    backgroundColor: "transparent"
  }
});
