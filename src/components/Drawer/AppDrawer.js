import React from "react";
import { Drawer } from "native-base";
import AppSideBar from "./AppSideBar";

export default class AppDrawer extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      drawerOpen: false,
      drawerDisabled: false
    };
  }

  closeDrawer = () => {
    this.drawer._root.close();
  };
  openDrawer = () => {
    this.drawer._root.open();
  };
  render() {
    return (
      <Drawer
        ref={ref => {
          this.drawer = ref;
        }}
        content={
          <AppSideBar
            navigator={this.navigator}
            closeDrawer={this.closeDrawer}
          />
        }
        type="overlay"
        // type="static"
        styles={{
          main: { shadowColor: "#000000", shadowOpacity: 0.3, shadowRadius: 15 }
        }}
        acceptDoubleTap
        onOpen={() => {
          this.setState({ drawerOpen: true });
        }}
        onClose={() => {
          this.setState({ drawerOpen: false });
        }}
        tweenDuration={100}
        panThreshold={0.08}
        openDrawerOffset={0.2}
        panOpenMask={0.8}
        negotiatePan
      >
        {this.props.children}
      </Drawer>
    );
  }
}
