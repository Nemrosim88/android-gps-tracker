import React from "react";
import { createStackNavigator } from "react-navigation";
import {Root, Button, Text} from 'native-base'
import HomeScreen from "./src/components/HomeScreen/HomeScreen";
import RegisterForm from "./src/components/RegisterScreen/RegisterScreen";
import LoginForm from "./src/components/LoginScreen/LoginScreen";
import LogoTitle from './src/components/LogoTitle';


const RootStack = createStackNavigator( 
  {
    Home: HomeScreen,
    RegisterForm: RegisterForm,
    LoginForm: LoginForm
  },
  {
    initialRouteName: "Home",
    navigationOptions: {
      headerTitle: <LogoTitle />,     
      headerStyle: {
        backgroundColor: "#114975"
      },
      headerTintColor: "#C16B00",
      headerTitleStyle: {
        fontWeight: "bold"
      }
    }
  }
);


export default class App extends React.Component {
  render() {
    return (
      <Root>
        <RootStack />
      </Root>
    );
  }
}
